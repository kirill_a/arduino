#include <ESP8266WiFi.h>// Import ESP8266 WiFi library
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
//#include <DHT.h>
#include <Wire.h> // Библиотека для работы с шиной I2C
#include <Adafruit_Si7021.h>


#define WLAN_SSID       "home_internal"
#define WLAN_PASS       "01020102"

#define AIO_SERVER      "192.168.5.2"
#define AIO_SERVERPORT  1883
//#define AIO_USERNAME    "ruzellramirez"
//#define AIO_KEY         "aio_WOPW54bbyKB0tzfIO33D6WFpynGq"


WiFiClient client;
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT);

Adafruit_MQTT_Publish Temperature = Adafruit_MQTT_Publish(&mqtt, "Temperature");
Adafruit_MQTT_Publish Humidity = Adafruit_MQTT_Publish(&mqtt, "Humidity");
Adafruit_MQTT_Subscribe LED = Adafruit_MQTT_Subscribe(&mqtt, "/feeds/LED");


Adafruit_Si7021 sensor;


void MQTT_connect();

void setup() {
  Serial.begin(115200);
  delay(10);

//  pinMode(D7, OUTPUT);
//  dht.begin();
  delay(10);

  Serial.println(); Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);

  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.println("WiFi connected");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());

  mqtt.subscribe(&LED);


  if(!sensor.begin()) { // Если датчик BMP280 не найден
    Serial.println("SENSOR ERROR"); // Выводим сообщение об ошибке
    while(1); // Переходим в бесконечный цикл
  }
}

void loop() {
  delay(10000);
  sensor = Adafruit_Si7021();  

  MQTT_connect();

  Adafruit_MQTT_Subscribe *subscription;
  if (! Temperature.publish(sensor.readTemperature())) {
    Serial.println(F("Temperature Failed"));
  } else {
    Serial.println(F("Temperature OK!"));
  }
    if (! Humidity.publish(sensor.readHumidity())) {
    Serial.println(F("Humidity Failed"));
  } else {
    Serial.println(F("Humidity OK!"));
  }
  delay(20000);
}

void MQTT_connect() 
{
  int8_t ret;
  if (mqtt.connected()) 
  {
    return;
  }

  Serial.print(" ");
  char strBuf[150];
  sprintf(strBuf, "Connecting to MQTT... %s:%d", AIO_SERVER, AIO_SERVERPORT);
  Serial.print(strBuf);
//  AIO_SERVERPORT

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect(); 
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         Serial.println("not connected after retries...");
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}
