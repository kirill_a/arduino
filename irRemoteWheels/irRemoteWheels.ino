//#define EN_WIFI
//
//#include <IRremoteESP8266.h>
//#include <IRrecv.h>


//#ifdef EN_WIFI
//#include <ESP8266WiFi.h>
//#include <ESP8266mDNS.h>
//#include <WiFiUdp.h>
//#include <ArduinoOTA.h>
//#endif
//
//const char* ssid = "home_internal";
//const char* password = "10072017";
 
const int MOTOR1_FORWARD = 12;
const int MOTOR1_BACKWARD = 1;
const int MOTOR2_FORWARD = 14;
const int MOTOR2_BACKWARD = 13;
const int RECV_PIN = 16;
const int SENSOR_ECHO_PIN = 4;
const int SENSOR_TRIG_PIN = 5;

const int CM_FWD = 16736925;
const int CM_LEFT = 16720605;
const int CM_RIGHT = 16761405;
const int CM_BKWD = 16754775;
const int CM_CENTR = 16712445;
const int CM_NO_SYGNAL = 99;

int lastIRCommand = 0;
int lastIRCommandCounter = 0;
bool isFirst = true;

//IRrecv irrecv(RECV_PIN);
 
//decode_results results;
 
void setup()
{
    pinMode(MOTOR1_FORWARD, OUTPUT);
    pinMode(MOTOR1_BACKWARD, OUTPUT);
    pinMode(MOTOR2_FORWARD, OUTPUT);
    pinMode(MOTOR2_BACKWARD, OUTPUT);
    pinMode(SENSOR_TRIG_PIN, OUTPUT);
    pinMode(SENSOR_ECHO_PIN, INPUT);
        
    Serial.begin(256400);
    Serial.println("setupWifi");
    ///setupWifi();
    
//    irrecv.enableIRIn(); // Start the receiver
    motorStop();
}

void setupWifi() {
#ifdef EN_WIFI
//  Serial.println("Booting");
//  WiFi.mode(WIFI_STA);
//  WiFi.begin(ssid, password);
//  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
//    Serial.println("Connection Failed! Rebooting...");
//    delay(5000);
//    ESP.restart();
//  }
//
//  ArduinoOTA.onStart([]() {
//    Serial.println("Start");
//  });
//  ArduinoOTA.onEnd([]() {
//    Serial.println("\nEnd");
//  });
//  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
//    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
//  });
//  ArduinoOTA.onError([](ota_error_t error) {
//    Serial.printf("Error[%u]: ", error);
//    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
//    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
//    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
//    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
//    else if (error == OTA_END_ERROR) Serial.println("End Failed");
//  });
//  ArduinoOTA.begin();
//  Serial.println("Ready");
//  Serial.print("IP address: ");
//  Serial.println(WiFi.localIP());
#endif
}

void distanse_sens(){
  long duration, distance;
  digitalWrite(SENSOR_TRIG_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(SENSOR_TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(SENSOR_TRIG_PIN, LOW);
  duration = pulseIn(SENSOR_ECHO_PIN, HIGH);
  distance = (duration/2) / 29.1;
  
  Serial.println(distance);
}

void loopWifi(){
#ifdef EN_WIFI
//  ArduinoOTA.handle();
#endif
}

void loop() {
  loopWifi();
  distanse_sens();
  process_wheels();
}

void process_wheels() {
	if (isFirst) {
	
		//domove(CM_LEFT);
		//delay(1800);
		//domove(CM_NO_SYGNAL);
		//delay(1200);
		domove(CM_BKWD);
		delay(2800);
//		parseSignal(CM_LEFT);
//		delay(300);
		
	}
	parseSignal(CM_NO_SYGNAL);
	isFirst = false;
//  if (irrecv.decode(&results)) {  
//    lastIRCommandCounter = 1;
//    parseSignal(results.value);
//    irrecv.resume(); // Receive the next value
//    delay(100);
//  } else {
//    lastIRCommandCounter++;
//    delay(10);
//  
//    if(lastIRCommandCounter > 10){
//        lastIRCommandCounter = 0;
//        parseSignal(CM_NO_SYGNAL);
//    }
//  }
}


void parseSignal(int val)
{
    if (val == -1){
        val = lastIRCommand;
    }
    int cmd;
    
    switch(val)   
    {  
        case CM_FWD:
            lastIRCommand = CM_FWD;
            cmd = lastIRCommand;
            break;
        case CM_LEFT:
            lastIRCommand = CM_LEFT;
            cmd = lastIRCommand;
            break;
        case CM_RIGHT:
            lastIRCommand = CM_RIGHT;
            cmd = lastIRCommand;
            break;
        case CM_BKWD:
            lastIRCommand = CM_BKWD;
            cmd = lastIRCommand;
            break;
        case CM_CENTR:
            lastIRCommand = CM_CENTR;
            cmd = lastIRCommand;
            break;
        default:
            lastIRCommand = CM_NO_SYGNAL;
            cmd = lastIRCommand;
            break;
    }
    domove(lastIRCommand);
}

void motorStop(){
    digitalWrite(MOTOR1_FORWARD, HIGH);
    digitalWrite(MOTOR2_FORWARD, HIGH);
    digitalWrite(MOTOR1_BACKWARD, HIGH);
    digitalWrite(MOTOR2_BACKWARD, HIGH);
}

void domove(int val){
    switch(val)   
    {  
        case CM_FWD:
//            Serial.println("forward");
            motorStop();
            digitalWrite(MOTOR1_FORWARD, LOW);
            digitalWrite(MOTOR2_FORWARD, LOW);
            break;
        case CM_LEFT:
            Serial.println("left");
            motorStop();
            digitalWrite(MOTOR1_FORWARD, LOW);
            digitalWrite(MOTOR2_BACKWARD, LOW);
            break;
        case CM_RIGHT:
            Serial.println("right");
            motorStop();
            //digitalWrite(MOTOR2_FORWARD, LOW);
            digitalWrite(MOTOR1_BACKWARD, LOW);
			digitalWrite(MOTOR2_BACKWARD, LOW);
//            digitalWrite(MOTOR1_BACKWARD, LOW);
            break;
        case CM_BKWD:
//       1     Serial.println("backward");
            motorStop();
            digitalWrite(MOTOR1_BACKWARD, LOW);
            digitalWrite(MOTOR2_BACKWARD, LOW);
            break;
        case CM_CENTR:
//            Serial.println("center");
            motorStop();
            break;
        case CM_NO_SYGNAL:
//            Serial.println("no sygnal");
            motorStop();
            break;
        default:
//            Serial.println("stop");
            motorStop();
    }
    
}

