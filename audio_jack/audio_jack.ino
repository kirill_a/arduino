#include "arduinoFFT.h"
#include <Servo.h>

arduinoFFT FFT = arduinoFFT(); /* Create FFT object */
Servo servo;

/*
These values can be changed in order to evaluate the functions
*/
#define CHANNEL A0
const uint16_t samples = 64; //This value MUST ALWAYS be a power of 2
const double samplingFrequency = 1000; //Hz, must be less than 10000 due to ADC

unsigned int sampling_period_us;
unsigned long microseconds;

/*
These are the input and output vectors
Input vectors receive computed results from FFT
*/
double vReal[samples];
double vImag[samples];

#define SCL_INDEX 0x00
#define SCL_TIME 0x01
#define SCL_FREQUENCY 0x02
#define SCL_PLOT 0x03

#define CMD_FWD 500
#define CMD_LEFT 400
#define CMD_RIGHT 200
#define CMD_BACK 300
#define CMD_STOP 150

#define SERVO_PIN 10
#define FWD_PIN  9
#define BCK_PIN 8
bool FWD_PIN_LEVEL = false;
bool BCK_PIN_LEVEL = false;



void setup()
{
    sampling_period_us = round(1000000*(1.0/samplingFrequency));
    Serial.begin(115200);
    servo.attach(SERVO_PIN);
//  Serial.println("Ready");
    pinMode(FWD_PIN, OUTPUT);
    pinMode(BCK_PIN, OUTPUT);

    FWD_PIN_LEVEL = false;
    BCK_PIN_LEVEL = false;
}

void loop()
{ 
    if (true) {
        /*SAMPLING*/
        for(int i=0; i<samples; i++)
        {
            microseconds = micros();    //Overflows after around 70 minutes!

            vReal[i] = analogRead(CHANNEL);
            vImag[i] = 0;
            while(micros() < (microseconds + sampling_period_us)) {
            //empty loop
            }
        }
        FFT.Windowing(vReal, samples, FFT_WIN_TYP_HAMMING, FFT_FORWARD);  /* Weigh data */
        FFT.Compute(vReal, vImag, samples, FFT_FORWARD); /* Compute FFT */
        FFT.ComplexToMagnitude(vReal, vImag, samples); /* Compute magnitudes */
        PrintVector(vReal, (samples >> 1), SCL_FREQUENCY);

        double x = FFT.MajorPeak(vReal, samples, samplingFrequency);
        if (isOK(vReal, (samples >> 1), SCL_FREQUENCY)) {
            Serial.println(x, 6); //Print out what frequency is the most dominant.
            do_cmd(x);
        }
    }
    output();
    
    //while(1); /* Run Once */
    //delay(200); /* Repeat after delay */
}

void output() {
    digitalWrite(FWD_PIN, LOW);
    digitalWrite(BCK_PIN, LOW);

    if (BCK_PIN_LEVEL) {
        digitalWrite(FWD_PIN, LOW);
        digitalWrite(BCK_PIN, HIGH);
    }
    if (FWD_PIN_LEVEL) {
        digitalWrite(FWD_PIN, HIGH);
        digitalWrite(BCK_PIN, LOW);
    }
}

bool frequencyLike(double freq, int likeVal){
    if ((freq > likeVal - 10) && (freq < likeVal + 10)) {
        return true;
    }
    return false;
}

void do_cmd(double freq) {
  
    if (frequencyLike(freq, CMD_FWD)) {
        servo.write(100);
        Serial.println("CMD_FWD");
        FWD_PIN_LEVEL = true;
        BCK_PIN_LEVEL = false;
    }

    if (frequencyLike(freq, CMD_LEFT)) {
        servo.write(140);
        Serial.println("CMD_LEFT");
    }

    if (frequencyLike(freq, CMD_RIGHT)) {
        servo.write(60);
        Serial.println("CMD_RIGHT");
    }

    if (frequencyLike(freq, CMD_BACK)) {
        servo.write(100);
        Serial.println("CMD_BACK");
        FWD_PIN_LEVEL = false;
        BCK_PIN_LEVEL = true;
    }

    if (frequencyLike(freq, CMD_STOP)) {
        servo.write(100);
        Serial.println("CMD_STOP");
        FWD_PIN_LEVEL = false;
        BCK_PIN_LEVEL = false;
    }
}


bool isOK(double *vData, uint16_t bufferSize, uint8_t scaleType)
{
    for (uint16_t i = 0; i < bufferSize; i++)
    {
        if (vData[i] > 250 && i >= 10) {
            return true;
        }
    }
    return false;
}

void PrintVector(double *vData, uint16_t bufferSize, uint8_t scaleType)
{
    return;
    for (uint16_t i = 0; i < bufferSize; i++)
    {
        double abscissa;
        /* Print abscissa value */
        switch (scaleType)
        {
            case SCL_INDEX:
                abscissa = (i * 1.0);
                break;
            case SCL_TIME:
                abscissa = ((i * 1.0) / samplingFrequency);
                break;
            case SCL_FREQUENCY:
                abscissa = ((i * 1.0 * samplingFrequency) / samples);
                break;
        }

      if (vData[i] > 250) {
          Serial.print(abscissa, 6);
          if(scaleType==SCL_FREQUENCY) {
              Serial.print("Hz");
          }
          Serial.print(" ");
          Serial.println(vData[i], 4);
      }
  }
  Serial.println();
}
