#include <Wire.h> // Библиотека для работы с шиной I2C
#include <SPI.h>  // Библиотека для работы с шиной SPI
#include <Adafruit_Si7021.h> // Библиотека для работы с датчиком BMP280

Adafruit_Si7021 sensor;


void setup() {
  Serial.begin(115200); // Для вывода отладочной информации в терминал
    delay(100);
    sensor = Adafruit_Si7021();

    
    if(!sensor.begin()) { // Если датчик BMP280 не найден
    Serial.println("SENSOR ERROR"); // Выводим сообщение об ошибке
    while(1); // Переходим в бесконечный цикл
  }
}
  
void loop() {
   Serial.print(F("Temperature "));
   Serial.println(sensor.readTemperature());
   Serial.print(F("humudity "));
   Serial.println(sensor.readHumidity());
    delay(5000);
}
